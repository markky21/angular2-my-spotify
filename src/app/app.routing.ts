import {MusicSearchComponent} from './music-search/music-search.component';
import {Page404Component} from './page404.component';
import {RouterModule, Routes} from '@angular/router';


const routesConfig: Routes = [
  {path: '', redirectTo: 'music', pathMatch: 'full'},
  {path: 'music', component: MusicSearchComponent},
  {path: '**', component: Page404Component, pathMatch: 'full'}
];
export const routerModule = RouterModule.forRoot(routesConfig, {
  enableTracing: false,
  useHash: false
});
