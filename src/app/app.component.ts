import {Component} from '@angular/core';
import {Http, Headers} from '@angular/http';
import {AuthService} from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  moduleId: module.id,
})
export class AppComponent {

  title = 'My Spotify';

  constructor(private http: Http, private authService: AuthService) {

  }

  ngOnInit() {
  }


}
