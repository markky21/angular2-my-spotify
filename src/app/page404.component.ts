import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'page404',
  template: `
    <h2 class="display-1"> page404 works!</h2>
  `,
  styles: []
})
export class Page404Component implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
