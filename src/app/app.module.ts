import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {MusicSearchModule} from './music-search/music-search.module';
import {AccountModule} from './account/account.module';
import {PlaylistsModule} from './playlists/playlists.module';

import {AuthService} from './auth.service';

import {Page404Component} from './page404.component';
import {AppComponent} from './app.component';

import {routerModule} from './app.routing';
import { NavBarComponent } from './nav-bar/nav-bar.component';

@NgModule({
  declarations: [
    AppComponent,
    Page404Component,
    NavBarComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AccountModule,
    PlaylistsModule,
    MusicSearchModule,
    routerModule
  ],
  providers: [
    AuthService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(private auth: AuthService) {

    auth.getTocken();

  }
}

