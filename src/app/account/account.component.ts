import {Component, OnInit} from '@angular/core';
import {AccountService} from './account.service';
import {AuthService} from '../auth.service';


@Component({
  selector: 'account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  userName = '';
  accountUrl = '';
  accountImage = '';

  constructor(private accountService: AccountService, private authService: AuthService) {
    this.authService.getUserAccountStream().subscribe(
      userAccount => {
        this.userName = userAccount.display_name || '';
        this.accountUrl = userAccount.external_urls.spotify || '';
        this.accountImage = userAccount.images["0"].url || '';
      });
  }

  ngOnInit() {
  }

}
