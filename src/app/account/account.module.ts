import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AccountComponent} from './account.component';

import {AccountService} from './account.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [AccountComponent],
  exports: [
    AccountComponent
  ],
  providers: [
    AccountService
  ]
})
export class AccountModule {
}
