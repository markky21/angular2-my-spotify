import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'nav-bar',
  template: `
    <nav class="navbar navbar-expand-lg navbar-light">
      <!--<a class="navbar-brand" href="#/">Music Search</a>-->

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" routerLink="/music" routerLinkActive="active">Album Search<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" routerLink="/playlist" routerLinkActive="active">Your Playlists</a>
          </li>
        </ul>
      </div>
    </nav>
  `,
  styles: []
})
export class NavBarComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
