import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'cart-edit',
  template: `
    <form #formRef="ngForm">
      <!--<div>valid {{formRef.valid}}</div>-->
      <!--<div>invalid {{formRef.invalid}}</div>-->
      <!--<div>touched {{formRef.touched}}</div>-->
      <!--<div>untouched {{formRef.untouched}}</div>-->
      <!--<div>dirty {{formRef.dirty}}</div>-->
      <!--<div>pristine {{formRef.pristine}}</div>-->
      <!--<div>submitted {{formRef.submitted}}</div>-->
      <div class="form-group">
        <h4>Playlist {{edited.name}}- Edit</h4>
      </div>
      <div class="form-group">
        <label>Name:</label>
        <input
          name="name"
          [(ngModel)]="edited.name"
          #nameRef="ngModel"
          [class.is-valid]="nameRef.valid"
          [class.is-invalid]="(nameRef.touched || formRef.submitted) && nameRef.invalid "
          required
          minlength="3"
          type="text" class="form-control"
        >
        <div class="invalid-feedback" *ngIf="nameRef.touched || formRef.submitted">
          <div *ngIf="nameRef.errors?.required" class="form-validation-error">* this field
            is required
          </div>
          <div *ngIf="nameRef.errors?.minlength" class="form-validation-error">* this field require min
            {{nameRef.errors?.minlength.requiredLength}} chars
          </div>
        </div>
      </div>
      <div class="form-group">
        <label>Description:</label>
        <textarea
          name="description"
          [(ngModel)]="edited.description"
          class="form-control"
          maxlength="200"
        ></textarea>
      </div>
      <div class="form-group">
        <label>Tracks:</label>
        <input
          name="tracks"
          [ngModel]="edited.tracks.length + ' tracks'"
          type="text"
          class="form-control"
          disabled>
      </div>
      <div class="form-group">
        <label>Color:
          <input
            name="color"
            [(ngModel)]="edited.color"
            type="color"
          >
        </label>
      </div>
      <div class="form-group">
        <label>
          <input
            name="favourite"
            [(ngModel)]="edited.favourite"
            type="checkbox"
          >
          Favourite
        </label>
      </div>
      <div class="form-group">
        {{formRef.valid}}
        <button class="btn btn-success float-right" (click)="save(formRef, edited)">Save</button>
      </div>
    </form>
  `,
  styles: [`
    textarea {
      min-height: 100px;
    }
  `]
})
export class CartEditComponent implements OnInit {

  @Input() edited = null;
  @Output() onSave = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  save(formRef, playlist) {
    console.log(formRef);
    if (!formRef.valid) {
      return;
    }
    this.onSave.emit(playlist);
  }
}
