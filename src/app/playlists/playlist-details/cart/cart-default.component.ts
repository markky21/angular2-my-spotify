import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'cart-default',
  template: `
    <div class="form-group">
      <h4>{{title}}</h4>
      <p>{{content}}</p>
    </div>
    <div class="form-group">
      <button class="btn btn-success float-right" (click)="editPlaylist('new')">New Playlist
      </button>
    </div>
  `,
  styles: []
})
export class CartDefaultComponent implements OnInit {
  @Input() title = '';
  @Input() content = '';
  @Input() modePlaylist = '';
  @Output() onEditPlaylist = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  editPlaylist() {
    this.onEditPlaylist.emit();
  }

}
