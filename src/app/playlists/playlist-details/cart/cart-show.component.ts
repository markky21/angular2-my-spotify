import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'cart-show',
  template: `
    <div class="form-group">
      <h4>Playlist {{selected.name}}</h4>
      <p>tracks: {{selected.tracks}}</p>
    </div>
    <div class="form-group">
      <button class="btn btn-success float-right" (click)="editPlaylist(selected)">Edit</button>
    </div>
  `,
  styles: []
})
export class CartShowComponent implements OnInit {

  @Input() selected = {};
  @Output() onEditPlaylist = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  editPlaylist(playlist) {
    this.onEditPlaylist.emit(playlist);
  }

}
