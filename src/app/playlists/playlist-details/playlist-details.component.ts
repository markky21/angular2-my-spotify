import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {PlaylistsService, Playlist} from '../playlists.service';
import {Router} from '@angular/router';

@Component({
  selector: 'playlist-details',
  template: `
    <div class="card">
      <div class="card-body">
        <div [ngSwitch]="modePlaylist">
          <cart-show *ngSwitchCase="'show'"
                     [selected]="selected"
                     (onEditPlaylist)="editPlaylist($event)"
          >
          </cart-show>
          <cart-edit *ngSwitchCase="'edit'"
                     [edited]="edited"
                     (onSave)="save($event)"
          ></cart-edit>
          <cart-default *ngSwitchDefault
                        title="Choose your playlist"
                        content="or create new one.."
                        (onEditPlaylist)="editPlaylist($event)"
          ></cart-default>
        </div>
      </div>
    </div>
  `,
  styles: []
})
export class PlaylistDetailsComponent implements OnInit {

  modePlaylist;
  id;
  mode;
  playlist;

  constructor(private activatedRoute: ActivatedRoute, private playlistsService: PlaylistsService, public router: Router) {

    this.activatedRoute.params.subscribe(params => {
      this.id = Number(params['id']);
      this.mode = params['mode'];

      switch (this.mode) {
        case 'new':
          this.editPlaylist();
          break;
        case 'show':
          if (this.id) {
            this.playlistsService.getPlaylist(this.id)
              .subscribe(playlist => this.selectPlaylist(playlist));
          } else {
            this.modePlaylist = null;
          }
          break;
        case 'edit':
          if (this.id) {
            this.playlistsService.getPlaylist(this.id)
              .subscribe(playlist => this.editPlaylist(playlist));
          } else {
            this.modePlaylist = null;
          }
          break;
        default:
          break;
      }
    });
  }

  playlists
  selected = {};
  edited = {};

  ngOnInit() {
    this.playlistsService.getPlaylistsStream()
      .subscribe((playlists: Playlist[]) => {
        this.playlists = playlists;
      });
  }


  editPlaylist(playlist = null) {
    if (playlist) {
      this.edited = Object.assign({}, playlist);
      this.router.navigate(['/playlist/edit', this.id]);
    } else {
      this.edited = this.playlistsService.getNewPlaylists();
      this.router.navigate(['/playlist/new']);
    }
    this.modePlaylist = 'edit';
  }

  selectPlaylist(playlist) {
    this.selected = playlist;
    this.modePlaylist = 'show';
  }

  save(playlist) {
    this.playlistsService.savePlaylist(playlist).subscribe(playlist => {
      if (this.id) {
        this.router.navigate(['/playlist/show', this.id]);
      } else {
        this.router.navigate(['/playlist/show', this.playlists[this.playlists.length - 1].id]);
      }
    });
  }

}
