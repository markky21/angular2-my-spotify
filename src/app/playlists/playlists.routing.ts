import {PlayListsComponent} from './playlists.component';
import {RouterModule, Routes} from '@angular/router';
import {PlaylistDetailsComponent} from './playlist-details/playlist-details.component';


const routesConfig: Routes = [
  {
    path: 'playlist', component: PlayListsComponent, children: [
    {path: '', component: PlaylistDetailsComponent},
    {path: ':mode', component: PlaylistDetailsComponent},
    {path: ':mode/:id', component: PlaylistDetailsComponent}
  ]
  },
];

export const routerModule = RouterModule.forChild(routesConfig);
