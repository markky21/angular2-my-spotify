import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Subject} from 'rxjs/Subject';

export interface Playlist {
  id: string,
  name: string,
  tracks: any[],
  color: string,
  favourite: boolean,
  description: string
}


@Injectable()
export class PlaylistsService {

  server_url = 'http://localhost:3000/playlists/';

  playlists: Playlist[];
  playlistsStream$ = new Subject<Playlist[]>();

  playlistConstructor = {
    id: null,
    name: '',
    tracks: [],
    color: '#000000',
    favourite: false,
    description: '',
  };

  constructor(private http: Http) {

  }


  getPlaylists() {
    return this.http.get(this.server_url)
      .map(response => response.json())
      .subscribe(playlists => {
        this.playlists = playlists;
        this.playlistsStream$.next(playlists);
      });
  }

  getPlaylistsStream() {
    if (!this.playlists.length) {
      this.getPlaylists();
    }
    return this.playlistsStream$.startWith(this.playlists);
  }

  getNewPlaylists(): Playlist {
    return Object.assign({}, this.playlistConstructor);
  }

  savePlaylist(playlist) {
    let request;
    if (playlist.id) {
      request = this.http.put(this.server_url + playlist.id, playlist);
    } else {
      request = this.http.post(this.server_url, playlist);
    }
    return request.map(response => response.json())
      .do(playlists => {
        this.getPlaylists();
      });
  }

  getPlaylist(id) {
    return this.http.get(this.server_url + id)
      .map(response => response.json());
  }

  addToPlaylist(playlistId, track) {
    const playlist = this.playlists.find(playlist => playlist.id == playlistId);
    playlist.tracks.push(track);
    this.savePlaylist(playlist)
      .subscribe(response => {
        // ...
      });
  }
}
