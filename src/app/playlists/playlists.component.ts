import {Component, OnInit, Inject} from '@angular/core';

@Component({
  selector: 'play-lists',
  template: `
    <div class="row">
      <div class="col">
        <h4 class="display-3 float-right mb-4">Your Playlists</h4>
      </div>
    </div>

    <div class="row">
      <div class="col-12 col-lg-6 mb-5">

        <playlist-table></playlist-table>

        <div class="form-group">
          <button class="btn btn-success float-right" routerLink="/playlist/new">New Playlist</button>
        </div>
      </div>

      <div class="col-12 col-lg-6 mb-5">
        <router-outlet></router-outlet>
      </div>
    </div>
  `,
  styleUrls: ['./playlists.component.scss']
})
export class PlayListsComponent implements OnInit {

  constructor() {

  }

  ngOnInit() {
  }

}
