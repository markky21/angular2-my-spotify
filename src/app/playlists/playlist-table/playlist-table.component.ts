import {Component, OnInit, Input} from '@angular/core';
import {PlaylistsService, Playlist} from '../playlists.service';

@Component({
  selector: 'playlist-table',
  template: `
    <table class="table table-striped playlist-list">
      <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">ID</th>
        <th scope="col">Name</th>
        <th scope="col">Tracks</th>
        <th scope="col">Favourite</th>
      </tr>
      </thead>
      <tbody>
      <tr *ngFor="let playlist of playlists; let i = index"
          [routerLink]="['/playlist', 'show', playlist.id]"
          [ngClass]="{'table-active': selected == playlist}"
      >
        <th [ngStyle]="{borderBottomColor: playlist.color}" scope="row">{{i + 1}}</th>
        <td [ngStyle]="{borderBottomColor: playlist.color}">{{playlist.id}}</td>
        <td [ngStyle]="{borderBottomColor: playlist.color}">{{playlist.name}}</td>
        <td [ngStyle]="{borderBottomColor: playlist.color}">{{playlist.tracks.length}}</td>
        <td [ngStyle]="{borderBottomColor: playlist.color}">
          <label>
            <input type="checkbox"
                   [(ngModel)]="playlist.favourite"
                   (click)="$event.stopPropagation()"
            >
            Favourite
          </label>
        </td>
      </tr>
      </tbody>
    </table>
  `,
  styles: [`
    th,
    td {
      border-bottom: 2px solid transparent;
    }
  `]
})
export class PlaylistTableComponent implements OnInit {

  @Input() selected = {};

  playlists;

  constructor(private playlistsService: PlaylistsService) {

  }

  ngOnInit() {
    this.playlistsService.getPlaylistsStream()
      .subscribe((playlists: Playlist[]) => {
        this.playlists = playlists;
      });
  }

}
