import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {AlertsModule} from '../alerts/alerts.module';

import {PlayListsComponent} from './playlists.component';
import {CartDefaultComponent} from './playlist-details/cart/cart-default.component';
import {CartEditComponent} from './playlist-details/cart/cart-edit.component';
import {CartShowComponent} from './playlist-details/cart/cart-show.component';
import {PlaylistTableComponent} from './playlist-table/playlist-table.component';
import {PlaylistDetailsComponent} from './playlist-details/playlist-details.component';
import {PlaylistsService} from './playlists.service';


import {routerModule} from './playlists.routing';

import playlistsData from './playlists.data';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BrowserModule,
    routerModule,
    AlertsModule,
  ],
  declarations: [
    PlayListsComponent,
    CartDefaultComponent,
    CartEditComponent,
    CartShowComponent,
    PlaylistTableComponent,
    PlaylistDetailsComponent,
  ],
  exports: [
    PlayListsComponent
  ],
  providers: [
    // {provide: PlaylistsService, useClass: ExtendedPlaylistsService} <--sposób rozwinięty
    PlaylistsService,
    {provide: 'PlaylistsData', useValue: playlistsData},
  ]
})
export class PlaylistsModule {
}
