import {Component, OnInit, Input, NgModule} from '@angular/core';

@Component({
  selector: 'main-alerts',
  template: `
    <div [ngSwitch]="alertType" class="alert-wrapper">
      <div *ngSwitchCase="'success'" class="alert alert-success">
        <strong>{{alertTitle}}</strong> {{alertDesc}}
      </div>

      <div *ngSwitchCase="'info'" class="alert alert-info">
        <strong>{{alertTitle}}</strong> {{alertDesc}}
      </div>

      <div *ngSwitchCase="'warning'" class="alert alert-warning">
        <strong>{{alertTitle}}</strong> {{alertDesc}}
      </div>

      <div *ngSwitchCase="'danger'" class="alert alert-danger">
        <strong>{{alertTitle}}</strong> {{alertDesc}}
      </div>
    </div>
  `,
  styles: []
})
export class AlertsComponent implements OnInit {

  @Input() alertType;
  @Input() alertTitle;
  @Input() alertDesc;

  constructor() {
  }

  ngOnInit() {
  }

}
