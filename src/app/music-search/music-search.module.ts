import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpModule} from '@angular/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {MusicSearchComponent} from './music-search.component';
import {AlbumListComponent} from './albums/album-list.component';
import {AlbumCartComponent} from './albums/album-card.component';

import {MusicSearchService} from './music-search.service';
import {AlbumSearchFormComponent} from './album-search-form/album-search-form.component';
import {routerModule} from './music-search.routing';
import { AlbumDetailsComponent } from './album-details/album-details.component';
import { AlbumCartBigComponent } from './album-details/album-cart-big/album-cart-big.component';
import { AlbumTrackListComponent } from './album-details/album-track-list/album-track-list.component';
import { AlbumArtistComponent } from './album-details/album-artist/album-artist.component';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    routerModule,
  ],
  declarations: [
    MusicSearchComponent,
    AlbumListComponent,
    AlbumCartComponent,
    AlbumSearchFormComponent,
    AlbumDetailsComponent,
    AlbumCartBigComponent,
    AlbumTrackListComponent,
    AlbumArtistComponent
  ],
  exports: [
    MusicSearchComponent,
    AlbumListComponent,
    AlbumCartComponent
  ],
  providers: [
    MusicSearchService
  ]
})
export class MusicSearchModule {
}
