import {Component, OnInit} from '@angular/core';
import {MusicSearchService} from '../music-search.service';

@Component({
  selector: 'album-list',
  template: `
    <div class="cart-deck row">
      <album-cart class="col-12 col-sm-4 mb-5"
                  *ngFor="let album of albums | async"
                  [album]="album"
                  [routerLink]="['/music', 'album', album.id]"
      >
      </album-cart>
    </div>
  `,
  styles: []
})
export class AlbumListComponent implements OnInit {

  albums;

  constructor(private musicSearchService: MusicSearchService) {
  }

  ngOnInit() {
   this.albums = this.musicSearchService.getAlbumsStream();
  }

}
