import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'album-cart',
  template: `
    <div class="card">
      <img class="card-img-top" [src]="image.url" alt="">
      <div class="card-body">
        <h4 class="card-title">{{album.name}}</h4>
      </div>
    </div>
  `,
  styleUrls: ['./album-card.component.scss'],
})
export class AlbumCartComponent implements OnInit {

  album = {};
  image = '';

  @Input('album')
  set setAlbum(album) {
    this.album = album;
    this.image = album.images[1];
  }

  constructor() {
  }

  ngOnInit() {
  }

}
