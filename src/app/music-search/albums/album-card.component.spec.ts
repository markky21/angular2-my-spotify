import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbumCartComponent } from './album-card.component';

describe('AlbumCartComponent', () => {
  let component: AlbumCartComponent;
  let fixture: ComponentFixture<AlbumCartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlbumCartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
