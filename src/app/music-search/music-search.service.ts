import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import '../rxjs-operators';

@Injectable()
export class MusicSearchService {

  albums = [];

  albumsStream = new Subject();

  constructor(private http: Http) {

    this.searchAlbum('eric clapton');

  }

  getAlbumsStream(): Observable<any> {
    return this.albumsStream.asObservable().startWith(this.albums);
  }

  searchAlbum(query) {
    let url = `https://api.spotify.com/v1/search?type=album&market=PL&query=${query}`;

    this.http.get(url)
      .map((response: Response) => {
        const data = response.json();
        return data.albums.items;
      })
      .do(albums => {
        this.albums = albums;
      })
      .subscribe((response: Response) => {
        this.albumsStream.next(this.albums);
      });
  }


  getArtist(ID) {
    let url = `https://api.spotify.com/v1/artists/${ID}`;

    return this.http.get(url)
      .map((response: Response) => response.json());
  }
  getAlbum(ID) {
    let url = `https://api.spotify.com/v1/albums/${ID}`;

    return this.http.get(url)
      .map((response: Response) => response.json());
  }
}
