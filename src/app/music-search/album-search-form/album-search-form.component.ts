import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';

import {MusicSearchService} from '../music-search.service';

@Component({
  selector: 'album-search-form',
  template: `
    <form [formGroup]="searchForm" class=" mb-5">

      <div class="input-group">
        <input formControlName="query" type="text"
               class="form-control" id="search-album-input"
               placeholder="Your album...">
      </div>

    </form>
  `,
  styles: []
})
export class AlbumSearchFormComponent implements OnInit {

  searchForm: FormGroup;

  constructor(private musicSearchService: MusicSearchService, private activatedRoute: ActivatedRoute) {
    const artistName = this.activatedRoute.snapshot.params['id'];
    if (artistName) {
      this.searchForm = new FormGroup({
        'query': new FormControl(artistName),
      });
    } else {
      this.searchForm = new FormGroup({
        'query': new FormControl('Eric Clapton'),
      });
    }

    this.searchForm.get('query').valueChanges
      .filter(query => query.length >= 3)
      .distinctUntilChanged()
      .debounceTime(400)
      .subscribe(query => {
        if (query && query !== '') {
          this.musicSearchService.searchAlbum(query);
        }
      });
  }

  ngOnInit() {

  }
}
