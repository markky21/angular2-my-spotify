import {Component, OnInit, Input} from '@angular/core';
import {PlaylistsService} from '../../../playlists/playlists.service';

@Component({
  selector: 'album-track-list',
  template: `
    <audio #AudioPlayer
           controls
           style="width: 100%"
           class="mb-3">
      Your browser does not support the audio element.
    </audio>

    <div *ngFor="let disc of tracksSeperated; let i = index" class="album-disc mb-5">
      <h3>Disc {{i + 1}}</h3>
      <table class="table table-striped playlist-list">
        <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Name</th>
          <th scope="col">Time</th>
          <th scope="col">Add to P</th>
        </tr>
        </thead>
        <tbody>
        <tr *ngFor="let track of disc;" (click)="play(AudioPlayer,track)">
          <th scope="row">{{track.track_number}}</th>
          <td>{{track.name}}</td>
          <td>{{millisToMinutesAndSeconds(track.duration_ms)}}</td>
          <td>Add</td>
        </tr>
        </tbody>
      </table>
    </div>
  `,
  styles: [`
    tr {
      cursor: pointer;
    }
  `]
})
export class AlbumTrackListComponent implements OnInit {

  tracks = {};
  tracksSeperated;

  @Input('album')
  set setAlbum(album) {
    this.tracks = album.tracks.items;
    this.seperateDiscs(this.tracks);
  }

  constructor() {
  }

  ngOnInit() {
  }

  millisToMinutesAndSeconds(millis) {
    const minutes = Math.floor(millis / 60000);
    const seconds = Number(((millis % 60000) / 1000).toFixed(0));
    return `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
  }

  seperateDiscs(tracks) {
    const resoult = [];
    for (let i = 0; i < tracks.length; i++) {

      const track = tracks[i];

      if (resoult[track.disc_number - 1] == null) {
        resoult.push([]);
      }
      resoult[track.disc_number - 1].push(track);
    }
    this.tracksSeperated = resoult;
  }

  play(AudioPlayer, track) {
    if (!AudioPlayer.classList.contains('activated')) {
      AudioPlayer.volume = 0.5;
      AudioPlayer.classList.add('activated');
    }
    if (AudioPlayer.src !== track.preview_url) {
      AudioPlayer.src = track.preview_url;
      AudioPlayer.play();
    } else if (AudioPlayer.paused) {
      AudioPlayer.play();
    } else {
      AudioPlayer.pause();
    }
  }
}
