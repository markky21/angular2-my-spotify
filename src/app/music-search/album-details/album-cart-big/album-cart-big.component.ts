import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'album-cart-big',
  template: `
    <div class="card">
      <img class="card-img-top" [src]="image" alt="">
      <div class="card-body">
        <h4 class="card-title">{{name}}</h4>
        <div class="album-additional-info">
          <p *ngIf="release_date" class="release-date"><span>release date: </span>{{release_date}}</p>
          <p *ngIf="label" class="album-label"><span>album label: </span>{{label}}</p>
        </div>
      </div>
    </div>
  `,
  styles: [`
    .album-additional-info p {
      margin-bottom: 5px;
    }

    .album-additional-info span {
      color: #999;
    }
  `]
})
export class AlbumCartBigComponent implements OnInit {

  name = '';
  image = '';
  release_date;
  label;


  @Input('album')
  set setAlbum(album) {

    this.image = album.images[1].url;
    this.name = album.name;
    this.release_date = album.release_date;
    this.label = album.label;
  }

  constructor() {
  }

  ngOnInit() {
  }

}
