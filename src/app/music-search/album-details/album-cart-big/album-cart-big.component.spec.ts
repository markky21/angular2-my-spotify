import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbumCartBigComponent } from './album-cart-big.component';

describe('AlbumCartBigComponent', () => {
  let component: AlbumCartBigComponent;
  let fixture: ComponentFixture<AlbumCartBigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlbumCartBigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumCartBigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
