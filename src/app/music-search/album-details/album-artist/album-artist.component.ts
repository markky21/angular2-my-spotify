import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'album-artist',
  template: `
    <div class="card">
      <div class="card-img">
        <img class="card-img-top" [src]="image" alt="">
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col">
            <h4 [routerLink]="['/music', 'artist-music', name]" class="card-title float-left">{{name}}</h4>
            <a [attr.href]="artistHref" target="_blank" class="float-right see-artist">
              see Artist on Spotify
            </a>
          </div>
        </div>
        <div class="clearfix"></div>

        <div class="followers mb-2">followers: <span>{{followers}}</span></div>

        <div class="genres">
          genres:
          <span *ngFor="let genre of genres">{{genre}}</span>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./album-artist.component.scss'],
})
export class AlbumArtistComponent implements OnInit {
  name = '';
  image = '';
  artistHref = '';
  genres = {};
  followers;
  id;

  @Input('artist')
  set setAlbum(artist) {

    this.image = artist.images[2].url;
    this.name = artist.name;
    this.id = artist.id;
    this.artistHref = artist.external_urls.spotify;
    this.genres = artist.genres;
    this.followers = artist.followers.total;
  }

  constructor() {
  }

  ngOnInit() {
  }
}
