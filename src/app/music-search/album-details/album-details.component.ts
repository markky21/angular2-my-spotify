import {Component, OnInit} from '@angular/core';
import {MusicSearchService} from '../music-search.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'album-details',
  template: `
    <div class="row">
      <div class="col">
        <h4 *ngIf="album" class="display-3 float-right mb-4">Album: {{album.name}}</h4>
      </div>
    </div>

    <div class="row mb-5">
      <div class="col">
        <album-artist *ngIf="artist" [artist]="artist"></album-artist>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-lg-6 mb-5">
        <album-cart-big *ngIf="album" [album]="album"></album-cart-big>
      </div>

      <div class="col-12 col-lg-6 mb-5">
        <album-track-list *ngIf="album" [album]="album"></album-track-list>
      </div>

    </div>
  `,
  styleUrls: ['./album-details.component.scss']
})
export class AlbumDetailsComponent implements OnInit {

  album;
  artist;

  constructor(private musicSearchService: MusicSearchService, private activatedRoute: ActivatedRoute) {

  }

  ngOnInit() {
    const albumId = this.activatedRoute.snapshot.params['id'];

    this.musicSearchService.getAlbum(albumId).subscribe(album => {
      this.album = album;
      //console.log('album: ', album);

      this.musicSearchService.getArtist(album.artists["0"].id).subscribe(artist => {
        this.artist = artist;
        // console.log('artist: ', artist);
      });

    });

  }

}
