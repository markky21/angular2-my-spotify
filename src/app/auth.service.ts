import {Injectable} from '@angular/core';
import {RequestOptions} from '@angular/http';
import {Http} from '@angular/http'
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class AuthService {

  userAccount = {};
  userAccountStream = new Subject();

  constructor(private requestOptions: RequestOptions, private http: Http) {
    this.getUserAccount();
    // this.instantCall();
  }

  getUserAccountStream(): Observable<any> {
    return this.userAccountStream.asObservable();
  }

  getUserAccount() {
    const token = this.getTocken();
    const url = 'https://api.spotify.com/v1/me';
    this.http.get(url, {}).subscribe(
      success => {
        this.userAccount = success.json();
        this.userAccountStream.next(success.json());
        return this.userAccount;
      },
      error => {
        if (error.status === 401) {
          this.getTocken(true);
        }
      }
    );
  }

  getTocken(newToken = false) {
    let token = localStorage.getItem('token');

    if (!token || token === 'null') {
      const match = window.location.hash.match(/#access_token=(.*?)&/);
      token = match && match[1];
      localStorage.setItem('token', token);
    }

    if (!token || token === 'null' || newToken) {
      this.authorize();
    }

    this.requestOptions.headers.set('Authorization', 'Bearer ' + token);

    return token;
  }

  authorize() {
    localStorage.removeItem('token');

    const clientId = '4c324cb184cf4d51be6f788d6b803586';
    const redirectUri = 'http://localhost:4200/';

    window.location.replace(`https://accounts.spotify.com/authorize?client_id=${clientId}&response_type=token&redirect_uri=${redirectUri}`);
  }

  instantCall() {
    const url = `https://api.spotify.com/v1/albums/3ebyEGol0Abc7VAxYf7vEg`;
    this.http.get(url, {}).subscribe(
      success => {
       console.log('instantCall: ', success.json());
      },
      error => {
        if (error.status === 401) {
          this.getTocken(true);
        }
      }
    );
  }
}
